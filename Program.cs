﻿internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Write <user> name");
        string user = Console.ReadLine();
        Console.WriteLine("Welcome to the programm " + user);
        Console.WriteLine("Goodbye " + user);
        Console.WriteLine("Hello " + user);
        Console.ReadLine();
    }
}